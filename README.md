# efax

This repository is a maintenance version of the efax program by Ed Casas.
This variant is based on version 0.9a-001144 of efax, a small ANSI C/POSIX
program that sends and receives faxes using any fax modem (Class 1, 2 or 2.0).

The original efax source tarball is located here [www.cce.com](https://www.cce.com/).

The patches applied here are various fixes collected from sources
on the web (bug trackers, other efax port maintainers, etc.) and my
own. I have given credit to the source for the patch if it was available.

Note that a number of fixes found on the web are not correct. Either they
introduce new defects or they don't properly address the reported issue.
I have **not** applied those patches that I deem to be lacking in some way.

This variant includes fixes to address the following:

* Fix from Debian bug #28027 - does not obey stdarg protocol, vfprintf destroys ap
* Fix from goOSe Linux when reading scan lines from a fax message
* Fix from Mandriva Linux - printf format specifiers
* Fix from PLD-Linux - null pointer checks
* Resolve SEGFAULTs caused by NULL pointers - Debian #476736, Red Hat #551708
* Resolve issue with uninitialized variable - from Steven Doerfler

See the efax README for additional information.

This variant is built and tested on [OpenBSD](https://www.openbsd.org) for i386 and amd64.
Users wishing to build and use this version on OpenBSD must also apply the OpenBSD
specific patches from [ports](https://cvsweb.openbsd.org/cgi-bin/cvsweb/ports/comms/efax/).
